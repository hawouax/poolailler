from django import forms
from PoolaillerDB.models import Pool, UserAccount, Team, Match
from django.forms import ModelForm
import datetime

class CreatePoolForm(forms.Form):
	Name = forms.CharField()
	MinBet = forms.IntegerField(label='Minimum Bet')
	EndDate = forms.DateField(initial=datetime.date.today)

class CreateModelMyPoolaillerForm(ModelForm):
	class Meta:
		model = UserAccount
		exclude = ['User', 'Tutorial', 'EggsCredit']

class CreateModelMatchForm(ModelForm):
	class Meta:
		model = Match
		exclude = ['IdMatch', 'Team']