from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Count
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from PoolaillerDB.models import Pool, Team, UserAccount, Match, TeamMatch, Bet, UserAccountPool, Competition
from PoolaillerDB.forms import CreatePoolForm, CreateModelMyPoolaillerForm, CreateModelMatchForm
import datetime

# Create your views here.
@login_required
def Pools_Index(request):
	pools = Pool.objects.annotate(UsersCount=Count('useraccountpool')).order_by('Name')

	if request.method == 'GET':
		if request.GET.get('MyPools') is not None:
			pools = Pool.objects.annotate(UsersCount=Count('useraccountpool')).filter(CreatedBy=request.user.username).order_by('Name')
		if request.GET.get('SoccerPools') is not None:
			pools = Pool.objects.annotate(UsersCount=Count('useraccountpool')).filter(Type='S').order_by('Name')
		if request.GET.get('HockeyPools') is not None:
			pools = Pool.objects.annotate(UsersCount=Count('useraccountpool')).filter(Type='H').order_by('Name')
		if request.GET.get('RugbyPools') is not None:
			pools = Pool.objects.annotate(UsersCount=Count('useraccountpool')).filter(Type='R').order_by('Name')
	userAccount = GetUserAccount(request.user.id)
	return render(request, 'Index.html', {'Title': 'Poolailler', 'Pools': pools, 'UserAccount': GetUserAccount(request.user.id), 'UserName': request.user.username})

@login_required
def CreatePool(request):
	message = None

	#todo replace variable 'MaxCreatedPools' by a settings
	MaxCreatedPools = 3
	userAccount = GetUserAccount(request.user.id)

	if ((Pool.objects.filter(CreatedBy=request.user.username).count() < MaxCreatedPools) or (request.user.is_superuser)):
		if request.method == 'POST':
			form = CreatePoolForm(data=request.POST)
			if form.is_valid():
				cd = form.cleaned_data
				if Pool.objects.filter(Name=cd['Name']).exists():
					message = 'Pool %s already exists.' % cd['Name']
				else:
					pool = Pool.objects.create(Name=cd['Name'], Type='H', Event='', MinBet=cd['MinBet'], TotalAmount=0, BeginDate=datetime.date.today(), EndDate=cd['EndDate'], CreatedBy=request.user.username)
					pool.user_id = request.user.id

					# retrieve the competition selected by the user
					competitionId = request.POST.get('Competition')
					
					if ((competitionId is not None) and (competitionId != '-1')):
						comp = Competition.objects.get(id=competitionId)
						if comp is None:
							# if no selected competition, user will have to add matches
							pool.save()
							return redirect('/AddMatch/' + str(pool.id))
						else:
							# User select a competition, add every match in the competition to the pool
							pool.Type = comp.Type
							for match in comp.Match.all():
								pool.Match.add(match)
							pool.save()
							return redirect('/JoinPool/' + str(pool.id))
					else:
						# if no selected competition, user will have to add matches
						pool.save()
						return redirect('/AddMatch/' + str(pool.id))
			else:
				message = form.errors
		else:
			form = CreatePoolForm()

		Competitions = Competition.objects.all()
		return render(request, 'CreatePool.html', {'form': form, 'message': message, 'Title': 'Create a Pool', 'CreatePoolActive' : 'active', 'UserAccount': userAccount, 'UserName': request.user.username, 'Competitions' : Competitions})
	else:
		messagesidebar = 'To create more than %d pools, please consider upgrading your account type to benefit our premium users advantages.' %MaxCreatedPools
		pools = Pool.objects.annotate(UsersCount=Count('useraccountpool')).order_by('Name')
		return render(request, 'Index.html', {'Title': 'Poolailler', 'Pools': pools, 'messagesidebar': messagesidebar, 'UserAccount': userAccount, 'UserName': request.user.username})

@login_required
def ViewPool(request, PoolId=""):
	pool = get_object_or_404(Pool, pk=PoolId)
	matches = pool.Match.prefetch_related('Team')
	Title = 'Viewing pool %s' % PoolId
	userAccount = GetUserAccount(request.user.id)	
	concurrents = UserAccountPool.objects.filter(Pool_id=PoolId).prefetch_related('UserAccount')	
	return render(request, 'ViewPool.html', {'Title': pool.Name, 'Pool': pool, 'Matches': matches, 'UserAccount': userAccount, 'UserName': request.user.username, 'Concurrents': concurrents})

@login_required
def JoinPool(request, PoolId=""):
	pool = get_object_or_404(Pool, pk=int(PoolId))
	matches = pool.Match.prefetch_related('Team')
	Title = 'Viewing pool %s' % PoolId
	IsOwner = ((request.user.username == pool.CreatedBy) or (request.user.is_superuser))
	userAccount = GetUserAccount(request.user.id)

	# feed user bets selection list using the element id of the html item we want to check
	selection = None
	uap = None
	try:
		uap = UserAccountPool.objects.get(UserAccount_id=userAccount.id, Pool_id=PoolId)
		selection = RetrieveBets(uap.id)
	except:
		pass

	return render(request, 'JoinPool.html', {'Title': pool.Name, 'Pool': pool, 'Matches': matches, 'UserAccount': userAccount, 'UserName': request.user.username, 'IsOwner': IsOwner, 'selection': selection})

def CreateOrGetTeam(TeamId, TeamName):
	team = None
	if TeamId != -1:
		team = Team.objects.get(id=TeamId)
	else:
		if TeamName != "":
			try:
				team = Team.objects.filter(Name__iexact=TeamName)[0]
			except:
				team = Team.objects.create(IdTeam="",Name=TeamName)
				team.save()
	return team
	
@login_required
def AddMatch(request, PoolId=""):
	message = None
	pool = get_object_or_404(Pool, pk=PoolId)
	if request.method == 'POST':
		form = CreateModelMatchForm(data=request.POST)
		if form.is_valid():
			cd = form.cleaned_data

			team1 = CreateOrGetTeam(request.POST.get('Team1', -1), request.POST.get('OtherTeam1', ''))
			team2 = CreateOrGetTeam(request.POST.get('Team2', -1), request.POST.get('OtherTeam2', ''))				

			if ((team1 is None) or (team2 is None)):
				message = 'Please select at least two teams.'
			else:
				if (team1==team2):
					message = 'Please select at two different teams.'					
				else:
					match = form.save(commit=False)
					match.IdMatch = 0
					match.user_id = request.user.id
					match.save()
					tm = TeamMatch(Team=team1, Match=match, Score=0)
					tm.save()
					tm = TeamMatch(Team=team2, Match=match, Score=0)
					tm.save()
					pool.Match.add(match)
					message = request.POST.urlencode()
					pool.save()
					return redirect('/JoinPool/' + PoolId)
		else:
			message = form.errors
	else:
		form = CreateModelMatchForm(initial={'Date': datetime.date.today, 'Hour': datetime.date.today})
	userAccount = GetUserAccount(request.user.id)
	teams = Team.objects.all()
	return render(request, 'AddMatch.html', {'form': form, 'message': message, 'Teams': teams, 'UserAccount': userAccount, 'UserName': request.user.username})

@login_required
def MyPoolailler(request):
	message = None
	userAccount = GetUserAccount(request.user.id)

	if request.method == 'POST':
		form = CreateModelMyPoolaillerForm(data=request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			userAccount.Language = form.data['Language']
			userAccount.CurrencyType = form.data['CurrencyType']
			userAccount.PaymentType = form.data['PaymentType']
			userAccount.save()
			return redirect('/')
	else:
		form = CreateModelMyPoolaillerForm(instance=userAccount)
		return render(request, 'MyPoolailler.html', {'form': form, 'message': message, 'UserAccount': userAccount, 'UserName': request.user.username})

@login_required
def PlaceBet(request, PoolId=""):
	message = None

	pool = get_object_or_404(Pool, pk=PoolId)
	userAccount = GetUserAccount(request.user.id)

	uap = None
	try:
		uap = UserAccountPool.objects.get(UserAccount_id=userAccount.id, Pool_id=PoolId)
	except:
		uap = UserAccountPool.objects.create(UserAccount_id=userAccount.id, Pool_id=PoolId)
		uap.save()

	ucString = request.POST.get("userChoices", "")
	userChoices = ucString.split(';')
	message = ''
	if ((len(userChoices) == 0) or (userChoices[0] == '')):
		message = 'No choice'

	bets = Bet.objects.filter(UserAccountPool_id=uap.id).prefetch_related('TeamMatch')
	for bet in bets:
		bet.delete()
		#redeem credits
		userAccount.EggsCredit += pool.MinBet

	for choice in userChoices:
		splitChoice = choice.split('_')
		if len(splitChoice) == 3:
			choiceMatchId = splitChoice[1]
			choiceTeamId = splitChoice[2]
			teamMatch = None
			try:
				teamMatch = TeamMatch.objects.get(Match_id=choiceMatchId,Team_id=choiceTeamId)
			except:
				pass
			if teamMatch is not None:
				bet = Bet.objects.create(TeamMatch_id=teamMatch.id, UserAccountPool_id=uap.id, Amount=0)
				bet.save()
				#decrease credits
				userAccount.EggsCredit -= pool.MinBet
	userAccount.save()

	matches = pool.Match.prefetch_related('Team')
	Title = 'Viewing pool %s' % PoolId

	# feed user bets selection list using the element id of the html item we want to check
	selection = RetrieveBets(uap.id)

	return render(request, 'JoinPool.html', {'Title': pool.Name, 'message': message, 'Pool': pool, 'Matches': matches, 'UserAccount': userAccount, 'selection': selection})

def RetrieveBets(UserAccountPoolId):
	# feed user bets selection list using the element id of the html item we want to check
	selection = []
	bets = Bet.objects.filter(UserAccountPool_id=UserAccountPoolId).prefetch_related('TeamMatch')
	for bet in bets:
		selection.append('UsrSel_' + str(bet.TeamMatch.Match_id) + '_' + str(bet.TeamMatch.Team_id))
	return selection

def GetUserAccount(usr_id):
	userAccount = None
	try:
		userAccount = UserAccount.objects.get(User_id=usr_id)
	except:
		useraccount = UserAccount.objects.create(User_id=user.usr_id)
		useraccount.save()
	return userAccount

@login_required
def DeletePool(request, PoolId=""):
	pool = get_object_or_404(Pool, pk=int(PoolId))
	pool.delete()
	return redirect('/')
	
def Parametres(request):
	message = None
	userAccount = GetUserAccount(request.user.id)

	if request.method == 'POST':
		form = CreateModelMyPoolaillerForm(data=request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			userAccount.Language = form.data['Language']
			userAccount.CurrencyType = form.data['CurrencyType']
			userAccount.PaymentType = form.data['PaymentType']
			userAccount.save()
			return redirect('/')
	else:
		form = CreateModelMyPoolaillerForm(instance=userAccount)
		return render(request, 'Parametre.html', {'form': form, 'message': message, 'UserAccount': userAccount, 'UserName': request.user.username})
