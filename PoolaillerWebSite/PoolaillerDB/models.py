from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class UserAccount(models.Model):
	LANGUAGE_TYPES = (
				('FRA', 'French'),
				('ENG', 'English'),
				('SPA', 'Spanish'),
			)

	CURRENCY_TYPES = (
				   ('CAD', 'Canada Dollar'),
				   ('USD', 'United States Dollar'),
				   ('MXN','Mexico Peso'),
				)

	User = models.ForeignKey(User)
	Language = models.CharField(max_length=3, choices=LANGUAGE_TYPES, default="FRA")
	EggsCredit = models.IntegerField(default=0)
	CurrencyType = models.CharField(max_length=3, choices=CURRENCY_TYPES, default="CAD")
	PaymentType = models.CharField(max_length=100, blank=True, null=True)
	Tutorial = models.IntegerField(default=0)
	
	def __str__(self):
		ret = "No Associated User"
		user = User.objects.get(id=self.User_id)
		if user is not None:
			ret = "User Account for " + user.username
		return ret

class Team(models.Model):
	IdTeam = models.CharField(max_length=3)
	Name = models.CharField(max_length=100)
	Country = models.CharField(max_length=100)
	Logo = models.BinaryField()

	def __str__(self):
		return self.Name

	class Meta:
		ordering = ['Name']

class Match(models.Model):
	IdMatch = models.IntegerField()
	Name = models.CharField(max_length=100, blank=True, null=True)
	Date = models.DateField()
	Hour = models.TimeField()
	Team = models.ManyToManyField(Team, through='TeamMatch')

	def __str__(self):
		return self.Name

class Pool(models.Model):
	POOL_TYPES = (
        ('F', 'Formula 1'),
		('H', 'Hockey'),
		('R', 'Rugby'),
		('S', 'Soccer'),
	)

	Name = models.CharField(max_length=100)
	Type = models.CharField(max_length=1, choices=POOL_TYPES)
	Event = models.CharField(max_length=100)
	BeginDate = models.DateField()
	EndDate = models.DateField()
	CreatedBy = models.CharField(max_length=100)
	MinBet = models.IntegerField()
	TotalAmount = models.IntegerField()
	Match = models.ManyToManyField(Match, blank=True)

	def __str__(self):
		return self.Name

	class Meta:
		ordering = ['Name']
		
class Competition(models.Model):
	Name = models.CharField(max_length=100)
	Type = models.CharField(max_length=1, choices=Pool.POOL_TYPES)
	Match = models.ManyToManyField(Match, blank=True)
	
	def __str__(self):
		return self.Name
		
	class Meta:
		ordering = ['Name']
		
class UserAccountPool(models.Model):
	UserAccount = models.ForeignKey(UserAccount)
	Pool = models.ForeignKey(Pool)

class TeamMatch(models.Model):
	Team = models.ForeignKey('Team')
	Match = models.ForeignKey('Match')
	Score = models.CharField(max_length=100, blank=True, null=True)
	
	def __str__(self):
		ret = ""
		match = Match.objects.get(id=self.Match_id)
		if match is not None:
			ret = "Match Id (" + str(match.id) + ")"
		team = Team.objects.get(id=self.Team_id)
		if team is not None:
			ret = ret + " for team " + team.Name
		if ret == "":
			ret = "No match yet or team yet"
		return ret

class Bet(models.Model):
	Amount = models.IntegerField()
	TeamMatch = models.ForeignKey(TeamMatch)
	UserAccountPool = models.ForeignKey(UserAccountPool)