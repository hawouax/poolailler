from django.contrib import admin
from PoolaillerDB.models import Pool, Team, Match, TeamMatch, Bet, UserAccount, Competition, UserAccountPool

# Register your models here.
admin.site.register(Pool)
admin.site.register(Team)
admin.site.register(Match)
admin.site.register(TeamMatch)
admin.site.register(Bet)
admin.site.register(UserAccount)
admin.site.register(Competition)
admin.site.register(UserAccountPool)