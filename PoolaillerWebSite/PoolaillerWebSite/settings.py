"""
Django settings for PoolaillerWebSite project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '*ru#v=)hg*#zlxxbt_exsn4n)8eqy@ha--duq#g4k*44$b7vy-'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'PoolaillerDB',
    #'south',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'PoolaillerWebSite.urls'

WSGI_APPLICATION = 'PoolaillerWebSite.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        # SQLite
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(os.path.join(os.path.dirname(__file__), 'Database').replace('\\','/'), 'db.sqlite3'),
        #MySQL
        #'ENGINE': 'django.db.backends.mysql',
        #'NAME': 'DATABASENAME',
        #'USER': 'DATABASEUSER',
        #'PASSWORD': 'PASSWORD',
    }
}
# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = (
	os.path.join(os.path.dirname(__file__), 'Static').replace('\\','/'),
)

# Template files (HTML)
# https://docs.djangoproject.com/en/dev/ref/templates/

TEMPLATE_DIRS = (
    os.path.join(os.path.dirname(__file__), 'Templates').replace('\\','/'),
)

LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'
