from django.conf.urls import patterns, include, url
from PoolaillerWebSite.views import SignUp, LogIn, LogOut, Error404
from PoolaillerDB.views import Pools_Index
from PoolaillerDB.views import CreatePool, ViewPool, AddMatch, MyPoolailler, JoinPool, PlaceBet, DeletePool, Parametres
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
	url(r'^$', Pools_Index),
	url(r'^signup/', SignUp),
	url(r'^login/', LogIn),
	url(r'^logout/', LogOut),
	url(r'^CreatePool/', CreatePool),
	url(r'^ViewPool/(?P<PoolId>\d+)/', ViewPool),
	url(r'^JoinPool/(?P<PoolId>\d+)/', JoinPool),
	url(r'^AddMatch/(?P<PoolId>\d+)/', AddMatch),
	url(r'^MyPoolailler/', MyPoolailler),
	url(r'^Parametres/', Parametres),
	url(r'^PlaceBet/(?P<PoolId>\d+)/', PlaceBet),
	url(r'^DeletePool/(?P<PoolId>\d+)/', DeletePool),
)

handler404 = Error404
