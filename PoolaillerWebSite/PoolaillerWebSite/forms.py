from django import forms

class SignUpForm(forms.Form):
	UserName = forms.CharField()
	EMail = forms.EmailField()
	Password = forms.CharField(widget=forms.PasswordInput())
	
class LoginForm(forms.Form):
	UserName = forms.CharField()
	Password = forms.CharField(widget=forms.PasswordInput())
