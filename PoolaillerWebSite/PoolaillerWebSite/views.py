from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from PoolaillerWebSite.forms import SignUpForm, LoginForm
from PoolaillerDB.models import UserAccount

def Error404(request):
	return render(request, '404.html')
	
def SignUp(request):
	message = None
	if request.method == 'POST':
		form = SignUpForm(request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			if User.objects.filter(username=cd['UserName']).exists():
				message = 'User %s already exists.' % cd['UserName']
			else:
				user = User.objects.create_user(username=cd['UserName'], email=cd['EMail'], password=cd['Password'])
				user.save()
				useraccount = UserAccount.objects.create(User_id=user.id)
				useraccount.save()
				return redirect('/login/')
	else:
		form = SignUpForm()
	return render(request, 'login.html', {'form': form, 'message': message, 'LoginView': 0})

def LogIn(request):
	message = None
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			user = authenticate(username=cd['UserName'], password=cd['Password'])
			if user is not None:
				if user.is_active:
					login(request, user)
					return redirect('/')
				else:
					message = 'User %s not active.' % cd['UserName']
			else:
				message = 'Invalid login information'
	else:
		form = LoginForm()
	return render(request, 'login.html', {'form': form, 'message': message, 'LoginView': 1})
	
def LogOut(request):
	logout(request)
	return redirect('/login/')

	
